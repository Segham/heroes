import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../menu";

export const UrsaMenu = BaseHeroMenu({
	NodeName: "Ursa",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	NpcName: "npc_dota_hero_ursa",
	Ability: [
		"ursa_overpower",
		"item_blink",
		"item_black_king_bar",
		"ursa_earthshock",
		"item_sheepstick",
		"item_abyssal_blade",
		"item_diffusal_blade",
		"item_orchid",
		"item_nullifier",
		"item_bloodthorn",
		"ursa_enrage",
		"item_mask_of_madness",
		"item_minotaur_horn",
		"item_shivas_guard",
		"item_medallion_of_courage",
		"item_solar_crest",
	],
	DistBlink: {default: 150},
})
