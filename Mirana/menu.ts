import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../menu";

export const MiranaMenu = BaseHeroMenu({
	NodeName: "Mirana",
	NpcName: "npc_dota_hero_mirana",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Ability: [
		"item_blink",
		"item_sheepstick",
		"item_cyclone",
		"item_wind_waker",
		"mirana_arrow",
		"mirana_starfall",
		"item_black_king_bar",
		"item_ethereal_blade",
		"item_veil_of_discord",
		"item_dagon_5",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"item_mask_of_madness",
		"item_minotaur_horn",
		"item_shivas_guard",
		"item_medallion_of_courage",
		"item_solar_crest",
	],
	DistBlink: {default: 450},
})
