import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../menu";

export const PugnaMenu = BaseHeroMenu({
	NodeName: "Pugna",
	NpcName: "npc_dota_hero_pugna",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Ability: [
		"item_blink",
		"item_black_king_bar",
		"pugna_decrepify",
		"item_ethereal_blade",
		"item_veil_of_discord",
		"item_dagon_5",
		"pugna_nether_blast",
		"pugna_life_drain",
		"item_sheepstick",
		"item_orchid",
		"item_nullifier",
		"item_bloodthorn",
		"item_minotaur_horn",
		"item_shivas_guard",
	],
	ExtendLinkenBreak: ["pugna_decrepify", "pugna_life_drain"],
})
