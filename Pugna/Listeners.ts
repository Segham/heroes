import { dotaunitorder_t, ExecuteOrder, Input, Unit, VKeys, VMouseKeys } from "wrapper/Imports";
import { OrbwalkerMapX } from "X-Core/Imports";
import { HERO_DATA } from "../_Core_/data";
import { PugnaCombo } from "../_Core_/Modules/Combo/Pugna";
import { ComboCallback } from "../_Core_/Modules/Tick";
import { HeroService } from "../_Core_/Service/Hero";
import { SwitchPanelService } from "../_Core_/Service/SwicthPanel";
import { TargetCombo } from "../_Core_/Service/Target";
import { PugnaMenu } from "./menu";

let ComboActived: boolean = false
let FastAblilitiesFilter: string[] = []

HeroService.RegisterHeroModule(PugnaMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onEnded,
	onKeyDown,
	onMouseDown,
})

function onTick(source: Unit) {
	if (!PugnaMenu.BaseState.value)
		return
	const NameMenu = PugnaMenu.PFastSwitchAbilities
	FastAblilitiesFilter = NameMenu.values.filter(x => NameMenu.IsEnabled(x))
	ComboCallback(source, TargetCombo, PugnaMenu, ComboActived,
		() => PugnaCombo(source, TargetCombo, PugnaMenu),
	)
}

function onDraw(unit: Unit) {
	if (!PugnaMenu.BaseState.value || unit.IsIllusion)
		return

	SwitchPanelService.Draw(PugnaMenu, unit, ...FastAblilitiesFilter)

	if (TargetCombo === undefined
		|| !unit.IsAlive
		|| !TargetCombo.IsAlive
		|| !TargetCombo.IsValid
		|| !TargetCombo.IsVisible
	) {
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${unit.Index}`)
		return
	}

	HERO_DATA.ParticleManager.DrawLineToTarget(`ATTACK_TARGET_${unit.Index}`, unit, TargetCombo,
		((PugnaMenu.StyleCombo.selected_id === 1 && !ComboActived) || (PugnaMenu.StyleCombo.selected_id === 0 && !PugnaMenu.ComboKey.is_pressed)
			? PugnaMenu.LineDeactive.selected_color
			: PugnaMenu.LineActive.selected_color
		),
	)
}

function onOrders(order: ExecuteOrder) {
	if (!PugnaMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	const Orbwalker = OrbwalkerMapX.get(order.Issuers[0])
	if (Orbwalker === undefined)
		return true

	Orbwalker.OnExecuteOrder(order, PugnaMenu.OrbwalkerSettings)

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& (PugnaMenu.StyleCombo.selected_id === 1 && ComboActived || (PugnaMenu.StyleCombo.selected_id === 0 && PugnaMenu.ComboKey.is_pressed)))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!PugnaMenu.BaseState.value)
		return true

	if (!Input.IsKeyDown(VKeys.CONTROL) && (PugnaMenu.ComboKey.assigned_key as VKeys) === key)
		return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!PugnaMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanelService.MouseLeftDown(PugnaMenu, ...FastAblilitiesFilter)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && (PugnaMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onEnded() {
	ComboActived = false
	FastAblilitiesFilter = []
}
