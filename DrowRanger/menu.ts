import { Attributes } from "wrapper/Imports"
import { BaseHeroMenu } from "../menu"

export let DrowMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_drow_ranger",
	NodeName: "Drow Ranger",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Ability: [
		"drow_ranger_frost_arrows",
		"item_blink",
		"item_sheepstick",
		"item_diffusal_blade",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"drow_ranger_wave_of_silence",
		"item_shivas_guard",
		"item_minotaur_horn",
		"item_black_king_bar",
		"item_medallion_of_courage",
		"item_solar_crest",
	],
	DistBlink: { default: 400 },
	FindRadius: { default: 300 },
	OrbwalkerSettings: {
		Move: { default: 60 },
		AttackDelay: { default: 20 },
		TurnDelay: { default: -30 },
		HoldRange: { default: 60 },
	},
})
