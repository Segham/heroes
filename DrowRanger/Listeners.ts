import { dotaunitorder_t, drow_ranger_frost_arrows, EntityManager, ExecuteOrder, Input, Unit, VKeys, VMouseKeys } from "wrapper/Imports"
import { OrbwalkerMapX } from "X-Core/Imports"
import { HERO_DATA } from "../_Core_/data"
import { DrowRangerCombo } from "../_Core_/Modules/Combo/DrowRanger"
import { ComboCallback } from "../_Core_/Modules/Tick"
import { HeroService } from "../_Core_/Service/Hero"
import { SwitchPanelService } from "../_Core_/Service/SwicthPanel"
import { TargetCombo } from "../_Core_/Service/Target"
import { DrowMenu } from "./menu"

HeroService.RegisterHeroModule(DrowMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onEnded,
	onKeyDown,
	onMouseDown,
})

let ComboActived: boolean = false
let FastAblilitiesFilter: string[] = []

DrowMenu.ComboKey.OnRelease(() => {
	ComboActived = !ComboActived
	if (!DrowMenu.BaseState.value)
		return
	EntityManager.GetEntitiesByClass(drow_ranger_frost_arrows).forEach(abil => {
		if (abil === undefined
			|| !DrowMenu.AbilityMenu?.IsEnabled(abil.Name)
			|| abil.Owner === undefined
			|| abil.Owner.IsEnemy()
			|| !abil.Owner.IsControllable)
			return
		abil.Owner.CastToggleAuto(abil)
	})
})

function onTick(source: Unit) {
	if (!DrowMenu.BaseState.value)
		return

	const NameMenu = DrowMenu.PFastSwitchAbilities
	FastAblilitiesFilter = NameMenu.values.filter(x => NameMenu.IsEnabled(x))

	ComboCallback(source, TargetCombo, DrowMenu, ComboActived,
		() => DrowRangerCombo(source, TargetCombo, DrowMenu))
}

function onDraw(unit: Unit) {
	if (!DrowMenu.BaseState.value || unit.IsIllusion)
		return

	SwitchPanelService.Draw(DrowMenu, unit, ...FastAblilitiesFilter)

	if (TargetCombo === undefined
		|| !unit.IsAlive
		|| !TargetCombo.IsAlive
		|| !TargetCombo.IsValid
		|| !TargetCombo.IsVisible
	) {
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${unit.Index}`)
		return
	}

	HERO_DATA.ParticleManager.DrawLineToTarget(`ATTACK_TARGET_${unit.Index}`, unit, TargetCombo,
		((DrowMenu.StyleCombo.selected_id === 1 && !ComboActived) || (DrowMenu.StyleCombo.selected_id === 0 && !DrowMenu.ComboKey.is_pressed)
			? DrowMenu.LineDeactive.selected_color
			: DrowMenu.LineActive.selected_color
		),
	)
}

function onOrders(order: ExecuteOrder) {
	if (!DrowMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	const Orbwalker = OrbwalkerMapX.get(order.Issuers[0])
	if (Orbwalker === undefined)
		return true

	Orbwalker.OnExecuteOrder(order, DrowMenu.OrbwalkerSettings)

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& (DrowMenu.StyleCombo.selected_id === 1 && ComboActived || (DrowMenu.StyleCombo.selected_id === 0 && DrowMenu.ComboKey.is_pressed)))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!DrowMenu.BaseState.value)
		return true

	if (!Input.IsKeyDown(VKeys.CONTROL) && (DrowMenu.ComboKey.assigned_key as VKeys) === key)
		return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!DrowMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanelService.MouseLeftDown(DrowMenu, ...FastAblilitiesFilter)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && (DrowMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onEnded() {
	ComboActived = false
	FastAblilitiesFilter = []
}
