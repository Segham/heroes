import { Attributes } from "wrapper/Imports"
import { BaseHeroMenu } from "../menu"

export const ClinkzMenu = BaseHeroMenu({
	NodeName: "Clinkz",
	NpcName: "npc_dota_hero_clinkz",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Ability: [
		"item_blink",
		"item_sheepstick",
		"item_orchid",
		"item_diffusal_blade",
		"item_abyssal_blade",
		"clinkz_searing_arrows",
		"item_nullifier",
		"item_bloodthorn",
		"clinkz_strafe",
		"item_minotaur_horn",
		"item_black_king_bar",
		"item_shivas_guard",
		"item_medallion_of_courage",
		"item_solar_crest",
	],
})
