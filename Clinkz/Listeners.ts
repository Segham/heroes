import { clinkz_searing_arrows, dotaunitorder_t, EntityManager, ExecuteOrder, Input, Unit, VKeys, VMouseKeys } from "wrapper/Imports"
import { OrbwalkerMapX } from "X-Core/Imports"
import { HERO_DATA } from "../_Core_/data"
import { BaseCombo } from "../_Core_/Modules/Combo/Base"
import { ComboCallback } from "../_Core_/Modules/Tick"
import { HeroService } from "../_Core_/Service/Hero"
import { SwitchPanelService } from "../_Core_/Service/SwicthPanel"
import { TargetCombo } from "../_Core_/Service/Target"
import { ClinkzMenu } from "./menu"

HeroService.RegisterHeroModule(ClinkzMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onEnded,
	onKeyDown,
	onMouseDown,
})

let ComboActived: boolean = false
let FastAblilitiesFilter: string[] = []

ClinkzMenu.ComboKey.OnRelease(() => {
	ComboActived = !ComboActived
	if (!ClinkzMenu.BaseState.value)
		return
	EntityManager.GetEntitiesByClass(clinkz_searing_arrows).forEach(abil => {
		if (abil === undefined
			|| !ClinkzMenu.AbilityMenu?.IsEnabled(abil.Name)
			|| abil.Owner === undefined
			|| abil.Owner.IsEnemy()
			|| !abil.Owner.IsControllable)
			return

		abil.Owner.CastToggleAuto(abil)
	})
})

function onTick(source: Unit) {
	if (!ClinkzMenu.BaseState.value)
		return

	const NameMenu = ClinkzMenu.PFastSwitchAbilities
	FastAblilitiesFilter = NameMenu.values.filter(x => NameMenu.IsEnabled(x))

	ComboCallback(source, TargetCombo, ClinkzMenu, ComboActived,
		() => BaseCombo(source, TargetCombo, ClinkzMenu),
	)
}

function onDraw(unit: Unit) {
	if (!ClinkzMenu.BaseState.value || unit.IsIllusion)
		return

	SwitchPanelService.Draw(ClinkzMenu, unit, ...FastAblilitiesFilter)

	if (TargetCombo === undefined
		|| !unit.IsAlive
		|| !TargetCombo.IsAlive
		|| !TargetCombo.IsValid
		|| !TargetCombo.IsVisible
	) {
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${unit.Index}`)
		return
	}

	HERO_DATA.ParticleManager.DrawLineToTarget(`ATTACK_TARGET_${unit.Index}`, unit, TargetCombo,
		((ClinkzMenu.StyleCombo.selected_id === 1 && !ComboActived) || (ClinkzMenu.StyleCombo.selected_id === 0 && !ClinkzMenu.ComboKey.is_pressed)
			? ClinkzMenu.LineDeactive.selected_color
			: ClinkzMenu.LineActive.selected_color
		),
	)
}

function onOrders(order: ExecuteOrder) {
	if (!ClinkzMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	const Orbwalker = OrbwalkerMapX.get(order.Issuers[0])
	if (Orbwalker === undefined)
		return true

	Orbwalker.OnExecuteOrder(order, ClinkzMenu.OrbwalkerSettings)

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& (ClinkzMenu.StyleCombo.selected_id === 1 && ComboActived || (ClinkzMenu.StyleCombo.selected_id === 0 && ClinkzMenu.ComboKey.is_pressed)))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!ClinkzMenu.BaseState.value)
		return true

	if (!Input.IsKeyDown(VKeys.CONTROL) && (ClinkzMenu.ComboKey.assigned_key as VKeys) === key)
		return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!ClinkzMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanelService.MouseLeftDown(ClinkzMenu, ...FastAblilitiesFilter)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && (ClinkzMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onEnded() {
	ComboActived = false
	FastAblilitiesFilter = []
}
