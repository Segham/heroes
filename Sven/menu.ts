import { BaseHeroMenu } from "../menu";

export const SvenMenu = BaseHeroMenu({
	NodeName: "Sven",
	NpcName: "npc_dota_hero_sven",
	Ability: [
		"sven_gods_strength",
		"item_black_king_bar",
		"sven_storm_bolt",
		"item_blink",
		"item_abyssal_blade",
		"item_sheepstick",
		"item_diffusal_blade",
		"item_orchid",
		"item_nullifier",
		"item_bloodthorn",
		"item_mask_of_madness",
		"item_minotaur_horn",
		"item_shivas_guard",
		"item_medallion_of_courage",
		"item_solar_crest",
		"sven_warcry",
	],
})
