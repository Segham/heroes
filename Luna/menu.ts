import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../menu";

export const LunaMenu = BaseHeroMenu({
	NodeName: "Luna",
	NpcName: "npc_dota_hero_luna",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Ability: [
		"item_blink",
		"luna_lucent_beam",
		"item_black_king_bar",
		"item_sheepstick",
		"item_ethereal_blade",
		"item_veil_of_discord",
		"item_dagon_5",
		"luna_eclipse",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"item_refresher",
		"item_mask_of_madness",
		"item_minotaur_horn",
		"item_shivas_guard",
		"item_medallion_of_courage",
		"item_solar_crest",
		"item_refresher_shard",
	],
	DistBlink: {default: 450},
	ExtendLinkenBreak: ["luna_lucent_beam"],
})
