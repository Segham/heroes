import { dotaunitorder_t, ExecuteOrder, Input, Unit, VKeys, VMouseKeys } from "wrapper/Imports";
import { OrbwalkerMapX } from "X-Core/Imports";
import { HERO_DATA } from "../_Core_/data";
import { LunaCombo } from "../_Core_/Modules/Combo/Luna";
import { ComboCallback } from "../_Core_/Modules/Tick";
import { HeroService } from "../_Core_/Service/Hero";
import { SwitchPanelService } from "../_Core_/Service/SwicthPanel";
import { TargetCombo } from "../_Core_/Service/Target";
import { LunaMenu } from "./menu";

let ComboActived: boolean = false
let FastAblilitiesFilter: string[] = []

HeroService.RegisterHeroModule(LunaMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onEnded,
	onKeyDown,
	onMouseDown,
})

function onTick(source: Unit) {
	if (!LunaMenu.BaseState.value)
		return
	const NameMenu = LunaMenu.PFastSwitchAbilities
	FastAblilitiesFilter = NameMenu.values.filter(x => NameMenu.IsEnabled(x))
	ComboCallback(source, TargetCombo, LunaMenu, ComboActived,
		() => LunaCombo(source, TargetCombo, LunaMenu),
	)
}

function onDraw(unit: Unit) {
	if (!LunaMenu.BaseState.value || unit.IsIllusion)
		return

	SwitchPanelService.Draw(LunaMenu, unit, ...FastAblilitiesFilter)

	if (TargetCombo === undefined
		|| !unit.IsAlive
		|| !TargetCombo.IsAlive
		|| !TargetCombo.IsValid
		|| !TargetCombo.IsVisible
	) {
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${unit.Index}`)
		return
	}

	HERO_DATA.ParticleManager.DrawLineToTarget(`ATTACK_TARGET_${unit.Index}`, unit, TargetCombo,
		((LunaMenu.StyleCombo.selected_id === 1 && !ComboActived) || (LunaMenu.StyleCombo.selected_id === 0 && !LunaMenu.ComboKey.is_pressed)
			? LunaMenu.LineDeactive.selected_color
			: LunaMenu.LineActive.selected_color
		),
	)
}

function onOrders(order: ExecuteOrder) {
	if (!LunaMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	const Orbwalker = OrbwalkerMapX.get(order.Issuers[0])
	if (Orbwalker === undefined)
		return true

	Orbwalker.OnExecuteOrder(order, LunaMenu.OrbwalkerSettings)

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& (LunaMenu.StyleCombo.selected_id === 1 && ComboActived || (LunaMenu.StyleCombo.selected_id === 0 && LunaMenu.ComboKey.is_pressed)))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!LunaMenu.BaseState.value)
		return true

	if (!Input.IsKeyDown(VKeys.CONTROL) && (LunaMenu.ComboKey.assigned_key as VKeys) === key)
		return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!LunaMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanelService.MouseLeftDown(LunaMenu, ...FastAblilitiesFilter)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && (LunaMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onEnded() {
	ComboActived = false
	FastAblilitiesFilter = []
}
