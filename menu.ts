import { Attributes, Color, Menu, Menu as MenuSDK } from "wrapper/Imports"
import { PathX } from "X-Core/Imports"

export const Base = MenuSDK.AddEntryDeep(["Heroes"])
export const StrengthMenu = Base.AddNode("Strength", PathX.Images.primary_attribute_strength)
export const AgilityMenu = Base.AddNode("Agility", PathX.Images.primary_attribute_agility)
export const IntellectMenu = Base.AddNode("Intellect", PathX.Images.primary_attribute_intelligence)

interface SliderComboMenu {
	min?: number,
	max?: number,
	default?: number,
}

export interface MenuCombo {
	BaseTree: MenuSDK.Node
	npc_name: string
	nodeName: string,
	BaseState: MenuSDK.Toggle
	SettingsTree: MenuSDK.Node
	ComboKey: MenuSDK.KeyBind
	ExtendBlink: MenuSDK.Slider,
	AbilityMenu: Nullable<MenuSDK.ImageSelector>,
	FindRadius: MenuSDK.Slider
	StyleCombo: MenuSDK.Dropdown
	FindTarget: MenuSDK.Dropdown
	LockerTarget: MenuSDK.Dropdown,
	VisualTree: MenuSDK.Node,
	CenterCamera: MenuSDK.Toggle,
	LinkenTree: MenuSDK.Node,
	LinkenState: MenuSDK.ImageSelector,
	PanelBKBSize: MenuSDK.Slider,
	PanelBKBPosistionX: MenuSDK.Slider,
	PanelBKBPosistionY: MenuSDK.Slider,
	PanelBKBState: MenuSDK.Toggle,
	LineActive: MenuSDK.ColorPicker,
	LineDeactive: MenuSDK.ColorPicker,
	PanelOpacityState: MenuSDK.Slider,
	PFastSwitchAbilities: MenuSDK.ImageSelector,
	OrbwalkerSettings: {
		State: MenuSDK.Toggle
		TurnDelay: MenuSDK.Slider
		MoveDelay: MenuSDK.Slider
		HoldRange: MenuSDK.Slider
		AttackDelay: MenuSDK.Slider,
	}
}

export interface MenuComboOption {
	NpcName: string
	NodeName: string,
	Ability: string[],
	NodeAttribute?: Attributes,
	DistBlink?: SliderComboMenu,
	FindRadius?: SliderComboMenu
	LockTarget?: number
	ExtendLinkenBreak?: string[],
	FindTargetDefault?: number,
	CenterCameraDefault?: boolean,
	OrbwalkerSettings?: {
		Move?: SliderComboMenu,
		AttackDelay?: SliderComboMenu,
		TurnDelay?: SliderComboMenu,
		HoldRange?: SliderComboMenu,
	}
}

export let LinkenItemsX: string[] = [
	"item_abyssal_blade",
	"item_book_of_shadows",
	"item_force_boots",
	"item_psychic_headband",
	"item_heavens_halberd",
	"item_diffusal_blade",
	"item_hurricane_pike",
	"item_dagon_5",
	"item_force_staff",
	"item_cyclone",
	"item_wind_waker",
	"item_orchid",
	"item_bloodthorn",
	"item_rod_of_atos",
	"item_nullifier",
	"item_ethereal_blade",
	"item_sheepstick",
]

const OrbwalkerCreateMenu = (node: Menu.Node, option: MenuComboOption) => {
	const State = node.AddToggle("State", true)
	const MoveDelay = node.AddSlider("Move Delay",
		option.OrbwalkerSettings?.Move?.default ?? 60,
		option.OrbwalkerSettings?.Move?.min ?? 0,
		option.OrbwalkerSettings?.Move?.max ?? 200,
	)
	const AttackDelay = node.AddSlider("Attack Delay",
		option.OrbwalkerSettings?.AttackDelay?.default ?? 20,
		option.OrbwalkerSettings?.AttackDelay?.min ?? 0,
		option.OrbwalkerSettings?.AttackDelay?.max ?? 200,
	)
	const TurnDelay = node.AddSlider("Turn Delay",
		option.OrbwalkerSettings?.TurnDelay?.default ?? 100,
		option.OrbwalkerSettings?.TurnDelay?.min ?? -200,
		option.OrbwalkerSettings?.TurnDelay?.max ?? 200,
	)
	const HoldRange = node.AddSlider("Hold Position Range",
		option.OrbwalkerSettings?.HoldRange?.default ?? 60,
		option.OrbwalkerSettings?.HoldRange?.min ?? 0,
		option.OrbwalkerSettings?.HoldRange?.max ?? 300,
	)
	return {
		State,
		TurnDelay,
		MoveDelay,
		HoldRange,
		AttackDelay,
	}
}

export const BaseHeroMenu = (option: MenuComboOption) => {

	const npc_name = option.NpcName
	const nodeName = option.NodeName

	let BaseTree!: MenuSDK.Node
	switch (option.NodeAttribute) {
		case Attributes.DOTA_ATTRIBUTE_AGILITY:
			BaseTree = AgilityMenu.AddNode(nodeName, PathX.Heroes(npc_name))
			break;
		case Attributes.DOTA_ATTRIBUTE_INTELLECT:
			BaseTree = IntellectMenu.AddNode(nodeName, PathX.Heroes(npc_name))
			break;
		default:
			BaseTree = StrengthMenu.AddNode(nodeName, PathX.Heroes(npc_name))
			break;
	}

	const BaseState = BaseTree.AddToggle("State")

	const ComboTree = option.Ability.length !== 0
		? BaseTree.AddNode("Combo")
		: undefined

	// TODO
	const LinkenTree = BaseTree.AddNode("Linken Breaker")
	const LinkenState = LinkenTree.AddImageSelector("ABILITIES_X_LINKEN", [
		...LinkenItemsX,
		...option.ExtendLinkenBreak ?? [],
	])

	const AbilityMenu = option.Ability.length !== 0
		? ComboTree!.AddImageSelector("ABILITIES_X", option.Ability)
		: undefined

	const ComboKey = option.Ability.length !== 0
		? ComboTree!.AddKeybind("Key for Combo", "F")
		: BaseTree.AddKeybind("Key for Combo", "F")

	const StyleCombo = option.Ability.length !== 0
		? ComboTree!.AddDropdown("Key Style", ["Hold", "Turn on/off"])
		: BaseTree.AddDropdown("Key Style", ["Hold", "Turn on/off"])

	const SettingsTree = BaseTree.AddNode("Settings")

	const CenterCamera = SettingsTree.AddToggle("Сenter camera",
		option.CenterCameraDefault ?? false,
		"Сenter camera on your hero.\nUsage only if 'Local Hero'",
	)

	const ExtendBlink = SettingsTree.AddSlider("Blink distance",
		option.DistBlink?.default ?? 300,
		option.DistBlink?.min ?? 0,
		option.DistBlink?.max ?? 1000,
		0, "Blink distance from enemy",
	)

	const FindTarget = SettingsTree.AddDropdown("Find target", ["Closest to mouse", "Closest by range"],
		option.FindTargetDefault ?? 0,
		"Technology to search enemy.\n- Closest to mouse: closest enemy to your mouse.\n- Closest by range: closest enemy to your hero.",
	)
	const FindRadius = SettingsTree.AddSlider("Closest range",
		option.FindRadius?.default ?? 200,
		option.FindRadius?.min ?? 100,
		option.FindRadius?.max ?? 2500,
		0, "The search radius to mouse/by range.\n- If you use closest range: set the value as greater than 600 for the best search.",
	)

	const LockerTarget = SettingsTree.AddDropdown("Target mode",
		["Free target", "Lock on target"],
		option.LockTarget ?? 1,
	)

	const OrbwalkerTree = BaseTree.AddNode("Orbwalker")
	const OrbwalkerSettings = OrbwalkerCreateMenu(OrbwalkerTree, option)

	// TODO
	const VisualTree = BaseTree.AddNode("Visual")
	const PanelBKBTree = VisualTree.AddNode("Switch panel")
	const PanelBKBState = PanelBKBTree.AddToggle("State", true, "Enable abilities and items in game")
	const fillerDefault = option.Ability.filter(x =>
		x === "item_black_king_bar" || x === "item_minotaur_horn" || x === "item_blink",
	)
	const PFastSwitchAbilities = PanelBKBTree.AddImageSelector("ABILITIES_X",
		option.Ability,
		new Map(fillerDefault.map(name => [name, true])),
	)
	const PanelBKBSize = PanelBKBTree.AddSlider("Size", 31.4, 12, 100, 1)
	const PanelOpacityState = PanelBKBTree.AddSlider("Opacity", 255, 100, 255)
	const PanelBKBPosistionX = PanelBKBTree.AddSlider("Position: X", 57.8, 1, 97, 1)
	const PanelBKBPosistionY = PanelBKBTree.AddSlider("Position: Y", 83.1, 1, 97, 1)

	const LineActive = VisualTree.AddColorPicker("Color combo", Color.Red, "Color enable combo")
	const LineDeactive = VisualTree.AddColorPicker("Color combo 2", Color.Yellow, "Color disable combo")

	return {
		ComboKey,
		npc_name,
		nodeName,
		BaseTree,
		BaseState,
		StyleCombo,
		FindTarget,
		AbilityMenu,
		SettingsTree,
		FindRadius,
		VisualTree,
		ExtendBlink,
		LockerTarget,
		OrbwalkerTree,
		CenterCamera,
		LinkenTree,
		LinkenState,
		OrbwalkerSettings,
		PanelBKBSize,
		PanelBKBPosistionX,
		PanelBKBPosistionY,
		PanelBKBState,
		LineActive,
		LineDeactive,
		PanelOpacityState,
		PFastSwitchAbilities,
	}
}

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Combo", "Комбо"],
	["Opacity", "Прозрачность"],
	["Hold", "Удерживать"],
	["Turn on/off", "Включить/Выключить"],
	["Key Style", "Стиль комбо"],
	["Key for Combo", "Кнопка для Комбо"],
	["Move Delay", "Задержка движения"],
	["Attack Delay", "Задержка атаки"],
	["Turn Delay", "Задержка поворота"],
	["Switch panel", "Панель переключения"],
	["Hold Position Range", "Диапазон удержания позиции"],
	["Color enable combo", "Цвет при активном комбо"],
	["Color disable combo", "Цвет при выключеном комбо"],
	["Color combo", "Цвет комбо"],
	["Color combo 2", "Цвет комбо 2"],
	["Panel switch BKB", "Панель переключения BKB"],
	["Find target", "Поиск цели"],
	["Free target", "Свободная цель"],
	["Closest range", "Радиус поиска"],
	["Lock on target", "Удержание цели"],
	["Closest to mouse", "Ближайший к мыши"],
	["Closest by range", "Ближайший по радиусу"],
	["Blink distance", "Дистанция блинка"],
	["Сenter camera", "Центрировать камеру"],
	["Target mode", "Режим цели"],
	["Linken Breaker", "Сбитие линки"],
	["Enable abilities and items in game", "Включение способностей и предметов в игре"],
	["Blink distance from enemy", "Дистанция блинка от противника"],
	["Сenter camera on your hero.\nUsage only if 'Local Hero'", "Центрировать камеру над вашим персонажем.\nИспользуется только если выбран 'Ваш Герой'"],
	["The search radius to mouse/by range.\n- If you use closest range: set the value as greater than 600 for the best search.", "Радиус поиска от мыши/вашего героя.\n- Если используете Ближайший по радиусу: установите значение больше 600 для лучшего поиска."],
	["Technology to search enemy.\n- Closest to mouse: closest enemy to your mouse.\n- Closest by range: closest enemy to your hero.", "Техника поиска противника.\n- Ближайший к мыши: ближайший противник к вашей мыши.\n- Ближайший по радиусу: ближайший противник к вашему персонажу."],
	["ABILITIES_X", ""],
	["ABILITIES_X_LINKEN", ""],
]))

MenuSDK.Localization.AddLocalizationUnit("english", new Map([
	["ABILITIES_X", ""],
	["ABILITIES_X_LINKEN", ""],
]))
