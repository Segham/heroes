import { EventsSDK } from "wrapper/Imports"
import { EntityX, GameX } from "X-Core/Imports"
import { HERO_DATA } from "../data"

EventsSDK.on("Tick", () => {
	if (!GameX.IsInGame)
		return
	EntityX.UnitsIsControllable.forEach(unit => {
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return
		hero_module.onTick(unit)
	})
})
