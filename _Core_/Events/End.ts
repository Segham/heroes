import { EntityX, EventsX } from "X-Core/Imports"
import { HERO_DATA } from "../data"

EventsX.on("GameEnded", () => {
	HERO_DATA.Dispose()
	EntityX.UnitsIsControllable.forEach(unit => {
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return
		hero_module.onEnded(unit)
	})
})
