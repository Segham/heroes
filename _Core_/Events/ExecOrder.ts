import { EventsSDK, Unit } from "wrapper/Imports"
import { GameX } from "X-Core/Imports"
import { HERO_DATA } from "../data"

EventsSDK.on("PrepareUnitOrders", order => {
	if (!(order.Issuers[0] instanceof Unit) || !GameX.IsInGame || order.Issuers[0].IsEnemy())
		return true
	const hero_module = HERO_DATA.HeroModules.get(order.Issuers[0].Name)
	if (hero_module === undefined)
		return true
	return hero_module.onOrders(order)
})
