import { DOTAGameUIState_t, GameState, InputEventSDK } from "wrapper/Imports"
import { EntityX, GameX } from "X-Core/Imports"
import { HERO_DATA } from "../data"

const IsValidKEY = () => {
	return GameX.IsInGame
		&& EntityX.UnitsIsControllable.length !== 0
		&& GameState.UIState === DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME
}

InputEventSDK.on("MouseKeyDown", key => {
	if (!IsValidKEY())
		return true
	return EntityX.UnitsIsControllable.some(unit => {
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return true
		return hero_module.onMouseDown(key)
	})
})

InputEventSDK.on("KeyDown", key => {
	if (!IsValidKEY())
		return true
	return EntityX.UnitsIsControllable.some(unit => {
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return true
		return hero_module.onKeyDown(key)
	})
})
