import { DOTAGameUIState_t, EventsSDK, GameState } from "wrapper/Imports"
import { EntityX, GameX } from "X-Core/Imports"
import { HERO_DATA } from "../data"

EventsSDK.on("Draw", () => {
	if (!GameX.IsInGame || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return
	EntityX.UnitsIsControllable.forEach(unit => {
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return
		hero_module.onDraw(unit)
	})
})
