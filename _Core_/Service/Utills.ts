import { Ability, item_arcane_blink, item_blink, item_dagon, item_fallen_sky, item_overwhelming_blink, item_swift_blink, Unit } from "wrapper/Imports"
import { MenuCombo } from "../../menu"

export class HERO_UTILITY {
	public static GetAbility(source: Unit, menu: MenuCombo) {
		return menu.AbilityMenu!.values.map(abilMenu => {
			if (!menu.AbilityMenu!.IsEnabled(abilMenu))
				return undefined

			if (!abilMenu.includes("item_"))
				return source.GetAbilityByName(abilMenu)

			let ability: Nullable<Ability>
			if (abilMenu.includes("blink")) {
				ability = source.GetItemByClass(item_fallen_sky)
					?? source.GetItemByClass(item_blink)
					?? source.GetItemByClass(item_swift_blink)
					?? source.GetItemByClass(item_arcane_blink)
					?? source.GetItemByClass(item_overwhelming_blink)
			} else if (abilMenu.includes("dagon")) {
				ability = source.GetItemByClass(item_dagon)
			} else
				ability = source.GetItemByName(abilMenu)

			return ability

		})
	}
}
