import { ArrayExtensions, GameState, Hero, Input, Menu, Unit, Vector3 } from "wrapper/Imports"
import { EntityX, PredictionX } from "X-Core/Imports"
import { MenuCombo } from "../../menu"

export let TargetCombo: Nullable<Unit>
export class TargetManager {

	public static get Prediction() {
		if (TargetCombo === undefined)
			return new Vector3()
		const BecameDormantTime = TargetCombo.BecameDormantTime
		if (TargetCombo.IsVisible)
			return TargetCombo
		const delay = GameState.RawGameTime - BecameDormantTime
		return PredictionX.PseudoPrediction(TargetCombo, delay)
	}

	public static orderByUnits(inRange: number = 1200, unit?: Nullable<Unit>) {
		const orderBy = unit !== undefined
			? unit.Position
			: Input.CursorOnWorld
		return ArrayExtensions.orderBy(EntityX.EnemyHeroes.filter(x => x.IsValid
			&& x.IsAlive
			&& !x.IsIllusion
			&& x.Distance2D(orderBy) <= inRange
			&& !this.CanReincarnationScepterActive(x),
		), x => x.Distance2D(orderBy))[0]
	}

	public static CanReincarnationScepterActive(unit: Unit) {
		return unit.ModifiersBook.HasBuffByName("modifier_skeleton_king_reincarnation_scepter_active")
	}

	public static Updateting = (source: Unit, menu: Nullable<MenuCombo>) => {
		TargetManager.Update(source,
			menu!.ComboKey,
			menu!.LockerTarget,
			menu!.FindTarget,
			menu!.FindRadius,
		)
		return TargetManager.IsRangeToTarget(source,
			menu!.LockerTarget,
			menu!.ComboKey,
			menu!.FindRadius,
			menu!.FindTarget,
		)
	}

	private static Update(
		unit: Unit,
		bind: Menu.KeyBind,
		loker: Menu.Dropdown,
		findTarget: Menu.Dropdown,
		findTargetRadius: Menu.Slider,
	) {

		const TargetUpdate = findTarget.selected_id === 0
			? TargetManager.orderByUnits(findTargetRadius.value)
			: TargetManager.orderByUnits(findTargetRadius.value, unit)

		if (loker.selected_id === 1 && (TargetCombo !== undefined
			? TargetManager.Locked(loker, bind)
			: !TargetManager.State(TargetUpdate, unit, findTargetRadius, findTargetRadius, findTarget)))
			return

		TargetCombo = TargetUpdate
	}

	private static IsRangeToTarget(
		unit: Unit,
		Locker: Menu.Dropdown,
		KeyBIND: Menu.KeyBind,
		findRadius: Menu.Slider,
		findTarget: Menu.Dropdown,
	) {
		if (TargetCombo === undefined)
			return false

		if (TargetCombo instanceof Hero && !TargetCombo.IsAlive) {
			TargetCombo = undefined
			return false
		}

		if (this.Locked(Locker, KeyBIND))
			return true

		return this.State(TargetCombo, unit, findRadius, findRadius, findTarget)
	}

	private static State(target: Unit | Vector3, unit: Unit, radius: Menu.Slider, findRadius: Menu.Slider, findTarget: Menu.Dropdown) {
		if (target === undefined)
			return false
		return findTarget.selected_id === 0
			? target.Distance2D(Input.CursorOnWorld) <= radius.value
			: unit.Distance2D(target) <= findRadius.value
	}

	private static Locked(Locker: Menu.Dropdown, KeyBIND: Menu.KeyBind) {
		return Locker.selected_id === 1 && KeyBIND.is_pressed && TargetCombo !== undefined
	}
}
