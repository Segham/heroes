import { Color, Input, LocalPlayer, Menu, RendererSDK, Unit, Vector2 } from "wrapper/Imports";
import { PathX } from "X-Core/Imports";
import { MenuCombo } from "../../menu";

export class SwitchPanelService {
	public static FilterMenu(menu: MenuCombo, state_items: string[]) {
		return menu.AbilityMenu?.values.filter(x => state_items.includes(x))
	}

	public static Draw(menu: MenuCombo, unit: Unit, ...state_items: string[]) {
		if (!menu.PanelBKBState.value || unit.Index !== LocalPlayer!.Hero!.Index)
			return
		const heroSize = new Vector2(menu.PanelBKBSize.value, menu.PanelBKBSize.value)
		const position = new Vector2(menu.PanelBKBPosistionX.value, menu.PanelBKBPosistionY.value)
		const image_position = RendererSDK.WindowSize.DivideScalar(100)
			.MultiplyForThis(position)
		this.FilterMenu(menu, state_items)?.forEach(name => {
			const Image = name.includes("item_")
				? PathX.DOTAItems(name)
				: PathX.DOTAAbilities(name)
			RendererSDK.Image(Image, image_position, -1, heroSize, Color.White.SetA(menu.PanelOpacityState.value))
			RendererSDK.OutlinedRect(image_position, heroSize, 2, menu.AbilityMenu?.IsEnabled(name) ?
				Color.Green.SetA(menu.PanelOpacityState.value) : Color.Red.SetA(menu.PanelOpacityState.value))
			image_position.AddForThis(new Vector2(heroSize.y + 2, 0))
		})
	}

	public static MouseLeftDown(menu: MenuCombo, ...state_items: string[]) {
		if (!menu.PanelBKBState.value)
			return
		const cursor = Input.CursorOnScreen
		const heroSize = new Vector2(menu.PanelBKBSize.value, menu.PanelBKBSize.value)
		const position = new Vector2(menu.PanelBKBPosistionX.value, menu.PanelBKBPosistionY.value)
		const image_position = RendererSDK.WindowSize.DivideScalar(100).MultiplyForThis(position)
		SwitchPanelService.FilterMenu(menu, state_items)?.forEach(name => {
			if (cursor.IsUnderRectangle(image_position.x, image_position.y, heroSize.x, heroSize.y + 2)) {
				menu.AbilityMenu?.enabled_values.set(name, !menu.AbilityMenu?.IsEnabled(name))
				Menu.Base.SaveConfigASAP = true
			}
			image_position.AddForThis(new Vector2(heroSize.y + 2, 0))
		})
	}
}
