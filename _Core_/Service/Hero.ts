import { item_sphere, Unit } from "wrapper/Imports"
import { HeroModule, HERO_DATA } from "../data"

export class HeroService {

	public static HasDebuffCaster(source: Unit) {
		const debuffChecker = source.ModifiersBook.GetAnyBuffByNames(HERO_DATA.DebuffCaster)
		if (debuffChecker === undefined)
			return false
		const abil = debuffChecker.Ability
		if (abil === undefined)
			return false
		return abil.Cooldown <= 4 || !abil.CanBeCasted()
	}

	public static RegisterHeroModule(name: string, module: HeroModule) {
		HERO_DATA.HeroModules.set(name, module)
	}

	public static HexTime(source: Nullable<Unit>) {
		if (source === undefined || this.HasDebuffCaster(source))
			return 0
		return source.ModifiersBook.GetAnyBuffByNames(HERO_DATA.DebuffHexed)?.RemainingTime ?? 0
	}

	public static SilencedTime(source: Nullable<Unit>) {
		if (source === undefined || this.HasDebuffCaster(source))
			return 0
		return source.ModifiersBook.GetAnyBuffByNames(HERO_DATA.DebuffSilenced)?.RemainingTime ?? 0
	}

	public static IsSpellShieldProtected(unit: Unit): boolean {
		return unit.ModifiersBook.HasAnyBuffByNames(HERO_DATA.BuffsShied)
	}

	public static IsLinkensProtected(unit: Unit) {
		const itemSphere = unit.GetItemByClass(item_sphere)
		return itemSphere !== undefined && itemSphere.Cooldown <= 0
			|| unit.HasBuffByName("modifier_item_sphere_target")
	}

	public static WinterCurseTime(source: Unit) {
		return source.ModifiersBook.GetBuffByName(HERO_DATA.WinterCurse)?.RemainingTime ?? 0
	}

	public static TimeWinter(source: Unit) {
		return source.ModifiersBook.GetAnyBuffByNames(HERO_DATA.Winter)?.RemainingTime ?? 0
	}

	public static MagicImmuneTime(source: Unit) {
		return source.ModifiersBook.GetAnyBuffByNames(HERO_DATA.BuffsMagicImmune)?.RemainingTime ?? 0
	}
}
