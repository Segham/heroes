import { ExecuteOrder, GameSleeper, ParticlesSDK, Unit, VKeys, VMouseKeys } from "wrapper/Imports"

export interface HeroModule {
	onOrders(order: ExecuteOrder): boolean
	onTick(unit: Unit): void
	onDraw(unit: Unit): void
	onEnded(unit: Unit): void
	onKeyDown(key: VKeys): boolean
	onMouseDown(key: VMouseKeys): boolean
}

export class HERO_DATA {

	public static Sleeper = new GameSleeper()
	public static ParticleManager = new ParticlesSDK()
	public static HeroModules = new Map<string, HeroModule>()

	public static CyclonebuffsArrays: string[] = [
		"modifier_eul_cyclone",
		"modifier_wind_waker",
	]

	public static BuffsShied = [
		"modifier_antimage_counterspell",
		"modifier_item_lotus_orb_active",
	]

	public static BuffsMagicImmune = [
		"modifier_black_king_bar_immune",
		"modifier_minotaur_horn_immune",
	]

	public static DebuffCaster = [
		"modifier_item_minotaur_horn",
		"modifier_item_black_king_bar",
		"modifier_item_lotus_orb",
		"modifier_item_manta_style",
		"modifier_item_cyclone",
	]

	public static DebuffHexed = [
		"modifier_lion_voodoo",
		"modifier_doom_bringer_doom",
		"modifier_sheepstick_debuff",
		"modifier_shadow_shaman_voodoo",
	]

	public static DebuffSilenced = [
		"modifier_silence",
		"modifier_riki_smoke_screen",
		"modifier_bloodthorn_debuff",
		"modifier_drowranger_wave_of_silence",
		"modifier_orchid_malevolence_debuff",
		"modifier_item_mask_of_madness_berserk",
		"modifier_abaddon_frostmourne_debuff_bonus",
		"modifier_silencer_global_silence",
		"modifier_night_stalker_crippling_fear",
		"modifier_grimstroke_ink_creature_debuff",
		"modifier_deathprophet_silence_debuff",
		"modifier_disruptor_static_storm",
		"modifier_skywrath_mage_ancient_seal",
		"modifier_earth_spirit_geomagnetic_grip_debuff",
	]

	public static Winter = [
		"modifier_winter_wyvern_winters_curse_aura",
		"modifier_winter_wyvern_cold_embrace",
	]

	public static WinterCurse = HERO_DATA.Winter[0]

	public static Dispose() {
		this.Sleeper.FullReset()
		this.DestroyAllParticle()
	}

	private static DestroyAllParticle() {
		this.ParticleManager.AllParticles.forEach(particle => {
			if (particle === undefined || !particle.IsValid)
				return
			particle.Destroy(true)
		})
	}
}
