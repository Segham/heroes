import { GameState, Unit, Vector3 } from "wrapper/Imports"
import { GameX, OrbwalkerMapX } from "X-Core/Imports"
import { MenuCombo } from "../../menu"
import { HERO_DATA } from "../data"
import { TargetManager } from "../Service/Target"

/**
 *
 * @param source | caster
 * @param enemy | target
 * @param menu | root menu combo
 * @param ComboActived | activated
 * @param queue_callback | array callback if return true tread tick stopped in this callback & work only this callback
 */
export function ComboCallback(
	source: Nullable<Unit>,
	enemy: Nullable<Unit>,
	menu: MenuCombo,
	ComboActived: boolean,
	...queue_callback: CallableFunction[]
) {

	if (!menu.BaseState.value || source === undefined || !source.IsAlive || source.IsIllusion)
		return

	if (!TargetManager.Updateting(source, menu))
		return

	const orbwalker = OrbwalkerMapX.get(source)

	if (orbwalker === undefined || enemy === undefined)
		return

	if ((menu.StyleCombo.selected_id === 1 && !ComboActived) || (menu.StyleCombo.selected_id === 0 && !menu.ComboKey.is_pressed))
		return

	if (menu.CenterCamera.value)
		GameState.ExecuteCommand("+dota_camera_center_on_hero")

	const Position = !(TargetManager.Prediction instanceof Vector3)
		? enemy.Position
		: TargetManager.Prediction

	const distance = enemy.Distance2D(source)
	const sleepMoveTime = 350 + (GameX.Ping / 1000)
	const sleepMoveName = `MoveComboSleep_${source.Index}_${enemy.Index}`

	if (HERO_DATA.Sleeper.Sleeping(sleepMoveName))
		return

	if (enemy.IsVisible && distance >= 1200) {
		source.MoveTo(enemy.Position)
		HERO_DATA.Sleeper.Sleep(sleepMoveTime, sleepMoveName)
		return
	}

	if (!enemy.IsVisible) {
		source.MoveTo(Position)
		HERO_DATA.Sleeper.Sleep(sleepMoveTime, sleepMoveName)
		return
	}

	if (queue_callback.some(call => call()))
		return

	const sleepBlinkName = `BlinkComboSleep_${source.Index}_${enemy.Index}`

	if (enemy.HasBuffByName("modifier_eul_cyclone") || HERO_DATA.Sleeper.Sleeping(sleepBlinkName))
		return

	if (menu.OrbwalkerSettings.State.value && orbwalker.OrbwalkTo(enemy, menu.OrbwalkerSettings))
		return
}

export function RunTimeCallback(
	source: Nullable<Unit>,
	enemy: Nullable<Unit>,
	menu: MenuCombo,
	IsActived: boolean,
	OrbWallkerState: boolean = false,
	...queue_callback: CallableFunction[]
) {
	if (!menu.BaseState.value || source === undefined || !source.IsAlive || source.IsIllusion)
		return

	if (!TargetManager.Updateting(source, menu))
		return

	const orbwalker = OrbwalkerMapX.get(source)

	if (orbwalker === undefined || enemy === undefined)
		return

	if (!IsActived)
		return

	const Position = !(TargetManager.Prediction instanceof Vector3)
		? enemy.Position
		: TargetManager.Prediction

	const distance = enemy.Distance2D(source)
	const sleepMoveTime = 350 + (GameX.Ping / 1000)
	const sleepMoveName = `MoveComboSleep_${source.Index}_${enemy.Index}`

	if (HERO_DATA.Sleeper.Sleeping(sleepMoveName))
		return

	if (enemy.IsVisible && distance >= 1200) {
		source.MoveTo(enemy.Position)
		HERO_DATA.Sleeper.Sleep(sleepMoveTime, sleepMoveName)
		return
	}

	if (!enemy.IsVisible) {
		source.MoveTo(Position)
		HERO_DATA.Sleeper.Sleep(sleepMoveTime, sleepMoveName)
		return
	}

	if (queue_callback.some(call => call()) || enemy.HasBuffByName("modifier_eul_cyclone") || !OrbWallkerState)
		return

	if (menu.OrbwalkerSettings.State.value && orbwalker.OrbwalkTo(enemy, menu.OrbwalkerSettings))
		return
}
