import { GameX } from "gitlab.com/FNT_Rework/x-core/Imports";
import { item_dagon, item_ethereal_blade, item_nullifier, item_rod_of_atos, Unit } from "wrapper/Imports";
import { MenuCombo } from "../../menu";
import { HERO_DATA } from "../data";
import { HeroService } from "../Service/Hero";

export const IsLinkenBreak = (owner: Unit, enemy: Unit, menu: MenuCombo) => {

	const IsEnabledState = menu.LinkenState.values.filter(x => menu.LinkenState.IsEnabled(x))
	const abilities = IsEnabledState.map(abilMenu => {
		if (!abilMenu.includes("item_"))
			return owner.GetAbilityByName(abilMenu)

		if (abilMenu.includes("dagon_"))
			return owner.GetItemByClass(item_dagon)

		return owner.GetItemByName(abilMenu)
	})

	if (!HeroService.IsLinkensProtected(enemy))
		return false

	abilities.forEach(abil => {
		if (abil === undefined || !abil.CanBeCasted())
			return false

		const sleepLinken = `SLEEP_ABILITY_LINKEN_${enemy.Index}_${owner.Index}`
		if (HERO_DATA.Sleeper.Sleeping(sleepLinken))
			return false

		const game_ping = (GameX.Ping / 1000)
		const is_projectile = abil instanceof item_rod_of_atos || abil instanceof item_ethereal_blade || abil instanceof item_nullifier

		const time = is_projectile
			? abil.GetHitTime(enemy.Position) + 150
			: (!abil.IsItem ? abil.GetCastDelay(enemy.Position, owner.FindRotationAngle(enemy) >= 0.15) + 50 : 350 + game_ping)
		abil.UseAbility(enemy)
		HERO_DATA.Sleeper.Sleep(time, sleepLinken)
	})

	return true
}
