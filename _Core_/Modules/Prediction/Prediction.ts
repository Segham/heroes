import { Unit } from "wrapper/Imports"

export function Predicted(enemy: Unit, delay: number) {
	const speed = enemy.IdealSpeed
	return enemy.Position.Add(enemy.Forward.MultiplyScalar(delay / 1000)
		.MultiplyScalar(speed))
}
