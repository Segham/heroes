import { DOTA_ABILITY_BEHAVIOR, Input, item_arcane_blink, item_black_king_bar, item_blink, item_dagon, item_ethereal_blade, item_fallen_sky, item_minotaur_horn, item_overwhelming_blink, item_rod_of_atos, item_sheepstick, item_swift_blink, skywrath_mage_concussive_shot, skywrath_mage_mystic_flare, SPELL_IMMUNITY_TYPES, Unit } from "wrapper/Imports"
import { GameX } from "X-Core/Imports"
import { MenuCombo } from "../../../menu"
import { DoubleUlti } from "../../../SkyWrathMage/menu"
import { HERO_DATA } from "../../data"
import { HeroService } from "../../Service/Hero"
import { HERO_UTILITY } from "../../Service/Utills"
import { Predicted } from "../Prediction/Prediction"
import { IsLinkenBreak } from "../Shield"

const awaitDebuffs: string[] = [
	"modifier_rod_of_atos_debuff",
	"modifier_skywrath_mage_concussive_shot_slow",
]

export const SkyWrathCombo = (owner: Unit, enemy: Nullable<Unit>, menu: MenuCombo) => {
	if (enemy === undefined || HeroService.WinterCurseTime(enemy) >= (1 / 3))
		return false

	const abilities = HERO_UTILITY.GetAbility(owner, menu).filter(x => x !== undefined && x.CanBeCasted())

	if (abilities.length === 0 && IsLinkenBreak(owner, enemy, menu))
		return false

	return abilities.some(abil => {
		if (abil === undefined)
			return false

		if (HeroService.WinterCurseTime(enemy) >= 0.4) {
			if (abil.CastPoint !== 0 && abil.IsInAbilityPhase)
				owner.OrderStop()
			return false
		}

		let setTime = enemy.IsMoving ? 300 : 0
		const hasScepter = owner.HasScepter || owner.HasBuffByName("modifier_item_ultimate_scepter_consumed")

		if (hasScepter && DoubleUlti.value)
			setTime = (enemy.IsMoving ? 700 : 520)

		const sleepName = `SLEEP_ABILITY_${enemy.Index}_${owner.Index}_`
		const awaitEshot = `SLEEP_ABILITY_ESHOT_${enemy.Index}_${owner.Index}`
		const awaitEblade = `SLEEP_ABILITY_EBLADE_${enemy.Index}_${owner.Index}`

		const game_ping = (GameX.Ping / 1000)
		const time = (!abil.IsItem ? abil.GetCastDelay(enemy.Position, owner.FindRotationAngle(enemy) >= 0.15) + 50 : 350 + game_ping)

		if (HERO_DATA.Sleeper.Sleeping(sleepName))
			return false

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_NO_TARGET)) {

			if ((abil.AOERadius !== 0 && !owner.IsInRange(enemy.Position, abil.AOERadius))
				|| abil.AOERadius === 0 && !owner.IsInRange(enemy.Position, owner.AttackRangeBonus(enemy))
				|| (enemy.IsMagicImmune && abil.AbilityImmunityType !== SPELL_IMMUNITY_TYPES.SPELL_IMMUNITY_ENEMIES_YES))
				return false

			if (abil instanceof skywrath_mage_concussive_shot && HeroService.HexTime(enemy) <= 0.3)
				HERO_DATA.Sleeper.Sleep(abil.GetHitTime(enemy.Position) + (owner.TurnRate(true) * 1000), awaitEshot)

			if (abil instanceof item_black_king_bar || abil instanceof item_minotaur_horn)
				if (HeroService.MagicImmuneTime(owner) >= 0.4)
					return false

			abil.UseAbility()
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_UNIT_TARGET)) {

			if (!abil.CanHit(enemy)
				|| HeroService.IsSpellShieldProtected(enemy)
				|| (enemy.IsMagicImmune && abil.AbilityImmunityType !== SPELL_IMMUNITY_TYPES.SPELL_IMMUNITY_ENEMIES_YES))
				return false

			if (IsLinkenBreak(owner, enemy, menu))
				return false

			if (abil instanceof item_sheepstick) {
				if (HeroService.HexTime(enemy) >= 0.4)
					return false

				const sleepSmartcast = `SLEEP_ABILITY_SMART_${enemy.Index}_${owner.Index}`
				if (HERO_DATA.Sleeper.Sleeping(sleepSmartcast))
					return false

				abil.UseAbility(enemy)
				HERO_DATA.Sleeper.Sleep(150 + game_ping, sleepSmartcast)
				return true
			}

			// hack time...
			if (abil instanceof item_rod_of_atos && HeroService.HexTime(enemy) <= 0.3)
				HERO_DATA.Sleeper.Sleep(abil.GetHitTime(enemy.Position) + (owner.TurnRate(true) * 1000), awaitEshot)

			if (abil instanceof item_ethereal_blade)
				HERO_DATA.Sleeper.Sleep(abil.GetHitTime(enemy.Position) + (owner.TurnRate(true) * 1000), awaitEblade)

			if (abil instanceof item_dagon)
				if (HERO_DATA.Sleeper.Sleeping(awaitEblade) && !enemy.IsEthereal)
					return false

			abil.UseAbility(enemy)
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_POINT)) {

			if (abil instanceof item_blink
				|| abil instanceof item_swift_blink
				|| abil instanceof item_arcane_blink
				|| abil instanceof item_overwhelming_blink
				|| abil instanceof item_fallen_sky
			) {

				const attackRange = owner.AttackRange
				if (owner.Distance2D(enemy) < attackRange)
					return false

				let blinkPos = enemy.Position.Extend(Input.CursorOnWorld, menu.ExtendBlink.value)
				if (owner.Distance2D(blinkPos) > abil.CastRange)
					blinkPos = owner.Position.Extend(blinkPos, abil.CastRange - 1)

				abil.UseAbility(blinkPos)
				HERO_DATA.Sleeper.Sleep(time, sleepName)
				return true
			}

			if (!abil.CanHit(enemy) || (enemy.IsMagicImmune && abil.AbilityImmunityType !== SPELL_IMMUNITY_TYPES.SPELL_IMMUNITY_ENEMIES_YES))
				return false

			if (abil instanceof skywrath_mage_mystic_flare) {
				if (HERO_DATA.Sleeper.Sleeping(awaitEshot) && !enemy.ModifiersBook.HasAnyBuffByNames(awaitDebuffs))
					return false

				if (enemy.HasBuffByName("modifier_rune_haste") && (!enemy.IsRooted || !enemy.IsStunned))
					return false

				abil.UseAbility(Predicted(enemy, setTime))
				HERO_DATA.Sleeper.Sleep(time, sleepName)
				return true
			}

			abil.UseAbility(enemy.Position)
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}
	})
}
