import { DOTA_ABILITY_BEHAVIOR, Input, item_arcane_blink, item_black_king_bar, item_blink, item_bloodthorn, item_cyclone, item_dagon, item_ethereal_blade, item_fallen_sky, item_minotaur_horn, item_orchid, item_overwhelming_blink, item_sheepstick, item_swift_blink, mirana_arrow, SPELL_IMMUNITY_TYPES, Unit } from "wrapper/Imports";
import { GameX } from "X-Core/Imports";
import { MenuCombo } from "../../../menu";
import { HERO_DATA } from "../../data";
import { HeroService } from "../../Service/Hero";
import { HERO_UTILITY } from "../../Service/Utills";
import { IsLinkenBreak } from "../Shield";

export const MiranaCombo = (owner: Unit, enemy: Nullable<Unit>, menu: MenuCombo) => {
	if (enemy === undefined)
		return false

	const abilities = HERO_UTILITY.GetAbility(owner, menu).filter(x => x !== undefined && x.CanBeCasted())
	const modifierCyclone = enemy.ModifiersBook.GetAnyBuffByNames(HERO_DATA.CyclonebuffsArrays)
	if (abilities.length === 0 && IsLinkenBreak(owner, enemy, menu))
		return false

	return abilities.some(abil => {
		if (abil === undefined)
			return false

		if (HeroService.TimeWinter(enemy) >= 0.4) {
			if (abil.CastPoint !== 0 && abil.IsInAbilityPhase)
				owner.OrderStop()
			return false
		}

		const sleepName = `SLEEP_ABILITY_${enemy.Index}_${owner.Index}_${abil.Index}`
		const awaitEblade = `SLEEP_ABILITY_EBLADE_${enemy.Index}_${owner.Index}`

		const game_ping = (GameX.Ping / 1000)
		const time = (!abil.IsItem ? abil.GetCastDelay(enemy.Position, owner.FindRotationAngle(enemy) >= 0.15) + 50 : 350 + game_ping)

		if (HERO_DATA.Sleeper.Sleeping(sleepName))
			return false

		if (modifierCyclone !== undefined && abil instanceof mirana_arrow) {
			if (owner.IsInAbilityPhase)
				return false

			const posTime = (owner.Distance2D(enemy) / abil.Speed) + abil.CastPoint
			if (abil.CanBeCasted() && (modifierCyclone.RemainingTime - game_ping) <= posTime) {
				abil.UseAbility(enemy.Position)
				HERO_DATA.Sleeper.Sleep(abil.GetCastDelay(enemy.Position, owner.FindRotationAngle(enemy) >= 0.15) + 50, sleepName)
				return true
			}

			if (owner.IsMoving)
				owner.OrderStop()

			return true
		}

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_NO_TARGET)) {

			if ((abil.AOERadius !== 0 && !owner.IsInRange(enemy.Position, abil.AOERadius))
				|| (abil.AOERadius === 0 && !owner.IsInRange(enemy.Position, owner.AttackRangeBonus(enemy))))
				return false

			if (abil instanceof item_black_king_bar || abil instanceof item_minotaur_horn) {
				if (HeroService.MagicImmuneTime(owner) >= 0.4)
					return false

				abil.UseAbility()
				HERO_DATA.Sleeper.Sleep(time, sleepName)
				return true
			}

			abil.UseAbility()
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_POINT)) {

			if (abil instanceof item_blink
				|| abil instanceof item_swift_blink
				|| abil instanceof item_arcane_blink
				|| abil instanceof item_overwhelming_blink
				|| abil instanceof item_fallen_sky
			) {

				const attackRange = owner.AttackRange
				if (owner.Distance2D(enemy) < attackRange)
					return false

				let blinkPos = enemy.Position.Extend(Input.CursorOnWorld, menu.ExtendBlink.value)
				if (owner.Distance2D(blinkPos) > abil.CastRange)
					blinkPos = owner.Position.Extend(blinkPos, abil.CastRange - 1)

				abil.UseAbility(blinkPos)
				HERO_DATA.Sleeper.Sleep(time, sleepName)
				return true
			}

			if (!abil.CanHit(enemy) || (enemy.IsMagicImmune && abil.AbilityImmunityType !== SPELL_IMMUNITY_TYPES.SPELL_IMMUNITY_ENEMIES_YES))
				return false

			if ((abil instanceof mirana_arrow) && (!owner.GetAbilityByClass(item_cyclone)?.CanBeCasted() || HeroService.WinterCurseTime(enemy) > 0))
				return false

			abil.UseAbility(enemy.Position) // todo if need prediction
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_UNIT_TARGET)) {

			if (!abil.CanHit(enemy)
				|| HeroService.IsSpellShieldProtected(enemy)
				|| (enemy.IsMagicImmune && abil.AbilityImmunityType !== SPELL_IMMUNITY_TYPES.SPELL_IMMUNITY_ENEMIES_YES))
				return false

			if (IsLinkenBreak(owner, enemy, menu))
				return false

			if (abil instanceof item_orchid || abil instanceof item_bloodthorn || abil instanceof item_sheepstick) {
				if (HeroService.SilencedTime(enemy) >= 0.4 || HeroService.HexTime(enemy) >= 0.4)
					return false

				const sleepSmartcast = `SLEEP_ABILITY_SMART_${enemy.Index}_${owner.Index}`
				if (HERO_DATA.Sleeper.Sleeping(sleepSmartcast))
					return false

				abil.UseAbility(enemy)
				HERO_DATA.Sleeper.Sleep(150 + game_ping, sleepSmartcast)
				return true
			}

			if (abil instanceof item_cyclone) {
				if (!owner.GetAbilityByClass(mirana_arrow)?.CanBeCasted() || HeroService.WinterCurseTime(enemy) > 0)
					return false

				// ...
				abil.UseAbility(enemy)
				HERO_DATA.Sleeper.Sleep(time, sleepName)
				return true
			}

			// hack time...
			if (abil instanceof item_ethereal_blade)
				HERO_DATA.Sleeper.Sleep(abil.GetHitTime(enemy.Position) + (owner.TurnRate(true) * 1000), awaitEblade)

			if (abil instanceof item_dagon)
				if (HERO_DATA.Sleeper.Sleeping(awaitEblade) && !enemy.IsEthereal)
					return false

			abil.UseAbility(enemy)
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}

		return false
	})
}
