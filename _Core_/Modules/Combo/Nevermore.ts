import { DOTA_ABILITY_BEHAVIOR, GameRules, GameState, Input, item_abyssal_blade, item_arcane_blink, item_black_king_bar, item_blink, item_bloodthorn, item_cyclone, item_ethereal_blade, item_minotaur_horn, item_orchid, item_overwhelming_blink, item_phase_boots, item_refresher, item_refresher_shard, item_sheepstick, item_spider_legs, item_swift_blink, Modifier, nevermore_requiem, nevermore_shadowraze1, nevermore_shadowraze2, nevermore_shadowraze3, PlayerResource, SignonState_t, Unit } from "wrapper/Imports";
import { GameX, PlayerX } from "X-Core/Imports";
import { MenuCombo } from "../../../menu";
import { ZXCPauseHeroState } from "../../../Nevermore/menu";
import { HERO_DATA } from "../../data";
import { HeroService } from "../../Service/Hero";
import { HERO_UTILITY } from "../../Service/Utills";
import { IsLinkenBreak } from "../Shield";

function GamePause(mod: Nullable<Modifier>, owner: Unit, enemy: Unit, game_ping: number) {

	if (PlayerResource === undefined)
		return

	const connection_state = PlayerResource.PlayerData
		.some(x => (x.ConnectionState as SignonState_t) !== SignonState_t.SIGNONSTATE_CONNECTED)

	if (!PlayerX.CanBeUsePause || owner.Distance2D(enemy) > 250 || GameRules?.IsPaused || mod === undefined)
		return

	const extedTime = (connection_state ? 0 : 6)
	if (!ZXCPauseHeroState.IsEnabled(enemy.Name)
		|| HERO_DATA.Sleeper.Sleeping("SLEEP_COMBO_PAUSE")
		|| (game_ping + mod.RemainingTime) > (0.15 + extedTime))
		return

	GameState.ExecuteCommand("dota_pause")
	HERO_DATA.Sleeper.Sleep(3000, "SLEEP_COMBO_PAUSE")
}

export const NevermoreCombo = (owner: Unit, enemy: Nullable<Unit>, menu: MenuCombo) => {
	if (enemy === undefined || enemy.IsMagicImmune || HeroService.WinterCurseTime(enemy) >= 3)
		return false

	const abilities = HERO_UTILITY.GetAbility(owner, menu).filter(x => x !== undefined && x.CanBeCasted())
	if (abilities.length === 0 && IsLinkenBreak(owner, enemy, menu))
		return false

	const debuff_await_time = 0.4
	const game_ping = GameX.Ping / 1000
	const sleepMoveTime = 250 + game_ping

	const modifierBash = enemy.GetBuffByName("modifier_bashed")
	const modifierCyclone = enemy.ModifiersBook.GetAnyBuffByNames(HERO_DATA.CyclonebuffsArrays)
	const sleepMoveName = `MoveComboSleep_${owner.Index}_${enemy.Index}`

	GamePause(modifierBash, owner, enemy, game_ping)
	GamePause(modifierCyclone, owner, enemy, game_ping)

	return abilities.some(abil => {
		if (abil === undefined)
			return false

		const sleepName = `SLEEP_ABILITY_${enemy.Index}_${owner.Index}_${abil.Index}`
		const sleepBlinkName = `BlinkComboSleep_${owner.Index}_${enemy.Index}`
		const sleepWinterName = `SLEEP_WINTER_${enemy.Index}_${owner.Index}`

		if (HERO_DATA.Sleeper.Sleeping(sleepName) || HERO_DATA.Sleeper.Sleeping(sleepWinterName))
			return false

		if (abil instanceof nevermore_shadowraze1 || abil instanceof nevermore_shadowraze2 || abil instanceof nevermore_shadowraze3)
			return false

		if (HeroService.WinterCurseTime(enemy) !== 0 && abil instanceof nevermore_requiem) {

			if (abil.CanBeCasted() && HeroService.WinterCurseTime(enemy) <= (abil.CastPoint + 0.02)) {
				abil.UseAbility(owner)
				const timeming = abil.GetCastDelay(enemy.Position, owner.FindRotationAngle(enemy) >= 0.15) + 50
				HERO_DATA.Sleeper.Sleep(timeming + 450, sleepWinterName)
				HERO_DATA.Sleeper.Sleep(timeming, sleepName)
				return true
			}

			if (!HERO_DATA.Sleeper.Sleeping(sleepMoveName) && owner.Distance2D(enemy) >= 15) {
				owner.MoveTo(enemy.Position)
				HERO_DATA.Sleeper.Sleep(sleepMoveTime, sleepMoveName)
				return false
			}
		}

		if (modifierBash !== undefined && modifierBash.RemainingTime >= 1 && abil instanceof nevermore_requiem) {

			if (owner.IsInAbilityPhase)
				return false

			const has_arcane = owner.GetAbilityByClass(item_arcane_blink)?.CanBeCasted()
			const sleepTimeBash = abil.GetCastDelay(enemy.Position, owner.FindRotationAngle(enemy) >= 0.15) + 50
			if (abil.CanBeCasted() && modifierBash.RemainingTime <= (!has_arcane ? 1.92 : 1)) {
				abil.UseAbility(owner)
				HERO_DATA.Sleeper.Sleep(sleepTimeBash, sleepName)
				return true
			}

			if (!HERO_DATA.Sleeper.Sleeping(sleepMoveName) && owner.Distance2D(enemy) >= 15) {
				owner.MoveTo(enemy.Position)
				HERO_DATA.Sleeper.Sleep(sleepMoveTime, sleepMoveName)
				return false
			}
		}

		if (modifierCyclone !== undefined) {

			const pos1 = owner.Position
			const pos2 = enemy.Position.Extend(pos1, 80)

			if (owner.IsInAbilityPhase)
				return false

			if (abil instanceof nevermore_requiem && abil.CanBeCasted() && (modifierCyclone.RemainingTime - game_ping) <= (abil.CastPoint + 0.03)) {
				abil.UseAbility()
				HERO_DATA.Sleeper.Sleep(abil.GetCastDelay(enemy.Position, owner.FindRotationAngle(enemy) >= 0.15) + 50, sleepName)
				return true
			}

			if (!HERO_DATA.Sleeper.Sleeping(sleepMoveName) && pos1.Distance2D(pos2) >= 75) {
				owner.MoveTo(pos2)
				const extendTime = owner.HasBuffByName("modifier_item_arcane_blink_buff")
				HERO_DATA.Sleeper.Sleep(sleepMoveTime + (extendTime ? 400 : 0), sleepMoveName)
				return false
			}
		}

		if (abil.IsInAbilityPhase || owner.IsInAbilityPhase || HERO_DATA.Sleeper.Sleeping(sleepName))
			return false

		const time = (!abil.IsItem ? abil.GetCastDelay(enemy.Position, owner.FindRotationAngle(enemy) >= 0.15) + 50 : 350 + game_ping)

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_NO_TARGET)) {
			if (abil instanceof nevermore_requiem || (abil instanceof item_black_king_bar && owner.Distance2D(enemy) >= 600))
				return false

			if ((abil instanceof item_black_king_bar || abil instanceof item_minotaur_horn) && HeroService.MagicImmuneTime(owner) >= debuff_await_time)
				return false

			if ((abil instanceof item_refresher_shard || abil instanceof item_refresher)
				&& owner.GetAbilityByClass(nevermore_requiem)?.CanBeCasted())
					return false

			abil.UseAbility(owner)
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_POINT)) {
			if (abil instanceof item_blink
				|| abil instanceof item_swift_blink
				|| abil instanceof item_arcane_blink
				|| abil instanceof item_overwhelming_blink
			) {
				const attackRange = owner.AttackRange
				if (owner.Distance2D(enemy) < attackRange)
					return false

				let blinkPos = enemy.Position.Extend(Input.CursorOnWorld, menu.ExtendBlink.value)
				if (owner.Distance2D(blinkPos) > abil.CastRange)
					blinkPos = owner.Position.Extend(blinkPos, abil.CastRange - 1)

				abil.UseAbility(blinkPos)
				HERO_DATA.Sleeper.Sleep(time, sleepName)
				HERO_DATA.Sleeper.Sleep(700 + game_ping, sleepBlinkName)
				return true
			}

			if (!abil.CanHit(enemy) || (owner.IsSilenced && !abil.IsItem))
				return false

			abil.UseAbility(enemy.Position) // todo if need prediction
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_UNIT_TARGET)) {

			if (!abil.CanHit(enemy) || HeroService.IsSpellShieldProtected(enemy))
				return false

			if (IsLinkenBreak(owner, enemy, menu))
				return false

			if (abil instanceof item_orchid || abil instanceof item_bloodthorn || abil instanceof item_sheepstick) {
				if (HeroService.SilencedTime(enemy) >= debuff_await_time
					|| HeroService.HexTime(enemy) >= debuff_await_time
					|| HeroService.TimeWinter(enemy) >= debuff_await_time + 0.1
				) return false

				abil.UseAbility(enemy)
				HERO_DATA.Sleeper.Sleep(time, sleepName)
				return true
			}

			if (abil instanceof item_ethereal_blade && abil.CanBeCasted())
				HERO_DATA.Sleeper.Sleep(abil.GetHitTime(enemy.Position) - game_ping, `SleepEblade_${owner.Index}_${enemy.Index}`)

			if (abil instanceof item_cyclone && HERO_DATA.Sleeper.Sleeping(`SleepEblade_${owner.Index}_${enemy.Index}`))
				return false

			if ((abil instanceof item_cyclone
				|| abil instanceof item_spider_legs
				|| abil instanceof item_phase_boots) && (modifierBash !== undefined
					|| !owner.GetAbilityByClass(nevermore_requiem)?.CanBeCasted()
					|| HeroService.WinterCurseTime(enemy) > 0)
				) return false

			if ((abil instanceof item_abyssal_blade
				|| abil instanceof item_spider_legs
				|| abil instanceof item_phase_boots) && (modifierCyclone !== undefined || HeroService.WinterCurseTime(enemy) > 0))
				return false

			if (abil instanceof item_abyssal_blade) {
				abil.UseAbility(enemy)
				HERO_DATA.Sleeper.Sleep(time, sleepName)
				HERO_DATA.Sleeper.Sleep(700 + game_ping, sleepBlinkName)
				return true
			}

			abil.UseAbility(enemy)
			HERO_DATA.Sleeper.Sleep(time, sleepName)
			return true
		}

		return false
	})
}
