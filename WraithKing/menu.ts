import { BaseHeroMenu } from "../menu";

export const WKingMenu = BaseHeroMenu({
	NodeName: "Wraith King",
	NpcName: "npc_dota_hero_skeleton_king",
	Ability: [
		"item_armlet",
		"item_blink",
		"item_abyssal_blade",
		"item_sheepstick",
		"skeleton_king_hellfire_blast",
		"item_diffusal_blade",
		"item_black_king_bar",
		"item_orchid",
		"item_nullifier",
		"item_bloodthorn",
		"skeleton_king_vampiric_aura",
		"item_mask_of_madness",
		"item_minotaur_horn",
		"item_shivas_guard",
		"item_medallion_of_courage",
		"item_solar_crest",
	],
	DistBlink: {default: 200},
	ExtendLinkenBreak: ["skeleton_king_hellfire_blast"],
})
