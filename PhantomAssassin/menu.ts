import { Attributes } from "wrapper/Imports"
import { BaseHeroMenu } from "../menu"

export const PAMenu = BaseHeroMenu({
	NodeName: "Phantom Assassin",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	NpcName: "npc_dota_hero_phantom_assassin",
	Ability: [
		"item_armlet",
		"phantom_assassin_stifling_dagger",
		"phantom_assassin_phantom_strike",
		"item_abyssal_blade",
		"item_sheepstick",
		"item_diffusal_blade",
		"item_black_king_bar",
		"item_orchid",
		"item_nullifier",
		"item_bloodthorn",
		"item_mask_of_madness",
		"item_minotaur_horn",
		"item_shivas_guard",
		"item_medallion_of_courage",
		"item_solar_crest",
	],
	ExtendLinkenBreak: [
		"phantom_assassin_stifling_dagger",
		"phantom_assassin_phantom_strike",
	],
})
