import { dotaunitorder_t, ExecuteOrder, Input, Unit, VKeys, VMouseKeys } from "wrapper/Imports"
import { OrbwalkerMapX } from "X-Core/Imports"
import { HERO_DATA } from "../_Core_/data"
import { BaseCombo } from "../_Core_/Modules/Combo/Base"
import { ComboCallback } from "../_Core_/Modules/Tick"
import { HeroService } from "../_Core_/Service/Hero"
import { SwitchPanelService } from "../_Core_/Service/SwicthPanel"
import { TargetCombo } from "../_Core_/Service/Target"
import { PAMenu } from "./menu"

HeroService.RegisterHeroModule(PAMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onEnded,
	onKeyDown,
	onMouseDown,
})

let ComboActived: boolean = false
let FastAblilitiesFilter: string[] = []

PAMenu.ComboKey.OnRelease(() => ComboActived = !ComboActived)

function onTick(source: Unit) {
	if (!PAMenu.BaseState.value)
		return

	const NameMenu = PAMenu.PFastSwitchAbilities
	FastAblilitiesFilter = NameMenu.values.filter(x => NameMenu.IsEnabled(x))

	ComboCallback(source, TargetCombo, PAMenu, ComboActived,
		() => BaseCombo(source, TargetCombo, PAMenu))
}

function onDraw(unit: Unit) {
	if (!PAMenu.BaseState.value || unit.IsIllusion)
		return

	SwitchPanelService.Draw(PAMenu, unit, ...FastAblilitiesFilter)

	if (TargetCombo === undefined
		|| !unit.IsAlive
		|| !TargetCombo.IsAlive
		|| !TargetCombo.IsValid
		|| !TargetCombo.IsVisible
	) {
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${unit.Index}`)
		return
	}

	HERO_DATA.ParticleManager.DrawLineToTarget(`ATTACK_TARGET_${unit.Index}`, unit, TargetCombo,
		((PAMenu.StyleCombo.selected_id === 1 && !ComboActived) || (PAMenu.StyleCombo.selected_id === 0 && !PAMenu.ComboKey.is_pressed)
			? PAMenu.LineDeactive.selected_color
			: PAMenu.LineActive.selected_color
		),
	)
}

function onOrders(order: ExecuteOrder) {
	if (!PAMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	const Orbwalker = OrbwalkerMapX.get(order.Issuers[0])
	if (Orbwalker === undefined)
		return true

	Orbwalker.OnExecuteOrder(order, PAMenu.OrbwalkerSettings)

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& (PAMenu.StyleCombo.selected_id === 1 && ComboActived || (PAMenu.StyleCombo.selected_id === 0 && PAMenu.ComboKey.is_pressed)))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!PAMenu.BaseState.value)
		return true

	if (!Input.IsKeyDown(VKeys.CONTROL) && (PAMenu.ComboKey.assigned_key as VKeys) === key)
		return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!PAMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanelService.MouseLeftDown(PAMenu, ...FastAblilitiesFilter)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && (PAMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onEnded() {
	ComboActived = false
	FastAblilitiesFilter = []
}
