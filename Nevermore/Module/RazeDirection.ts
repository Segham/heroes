import { Ability, Input, Unit } from "wrapper/Imports";
import { ComboActived } from "../Listeners";
import { ZXCMenu } from "../menu";

export const RazeDirection = (abil: Ability, owner: Unit) => {
	const pos = Input.CursorOnWorld
	const rotation = owner.FindRotationAngle(pos)
	if (rotation <= 0.15
		|| (ZXCMenu.StyleCombo.selected_id === 1 && ComboActived || (ZXCMenu.StyleCombo.selected_id === 0 && ZXCMenu.ComboKey.is_pressed)))
		return true
	owner.MoveToDirection(pos)
	owner.CastNoTarget(abil)
	return false
}
