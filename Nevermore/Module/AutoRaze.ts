import { Ability, item_cyclone, nevermore_shadowraze1, nevermore_shadowraze2, nevermore_shadowraze3, Unit } from "wrapper/Imports";
import { PredictionX } from "X-Core/Imports";
import { MenuCombo } from "../../menu";
import { HERO_DATA } from "../../_Core_/data";
import { HERO_UTILITY } from "../../_Core_/Service/Utills";

function PredictionRize(abil: Ability, owner: Unit, enemy: Unit) {
	if (enemy.HasBuffByName("modifier_nevermore_requiem_fear"))
		return enemy.Distance2D(owner.InFront(abil.CastRange)) <= abil.AOERadius + enemy.HullRadius

	return PredictionX.PseudoPrediction(enemy, abil.GetCastDelay(enemy.Position, true))
		.Distance2D(owner.InFront(abil.CastRange)) <= abil.AOERadius + enemy.HullRadius
}

export const NevermoreAutoRaze = (owner: Unit, enemy: Nullable<Unit>, menu: MenuCombo) => {
	if (enemy === undefined
		|| enemy.IsMagicImmune
		|| owner.IsInAbilityPhase
		|| enemy.HasBuffByName("modifier_winter_wyvern_winters_curse_aura")
	) return

	const cyclone = owner.GetItemByClass(item_cyclone)
	const sleepBlinkName = `BlinkComboSleep_${owner.Index}_${enemy.Index}`
	const sleepAnyAbility = `SLEEP_ABILITY_${enemy.Index}_${owner.Index}_${cyclone?.Index}`

	if (enemy.HasBuffByName("modifier_eul_cyclone")
		|| HERO_DATA.Sleeper.Sleeping(sleepBlinkName)
		|| HERO_DATA.Sleeper.Sleeping(sleepAnyAbility))
		return

	HERO_UTILITY.GetAbility(owner, menu).filter(x => x !== undefined && x.CanBeCasted()).forEach(abil => {
		if (!(abil instanceof nevermore_shadowraze1 || abil instanceof nevermore_shadowraze2 || abil instanceof nevermore_shadowraze3))
			return

		if (!PredictionRize(abil, owner, enemy) || HERO_DATA.Sleeper.Sleeping(`SLEEP_ABILITY_${enemy.Index}_${owner.Index}_${abil.Index}`))
			return

		abil.UseAbility()
		HERO_DATA.Sleeper.Sleep(abil.GetCastDelay(enemy.Position, true) + 50, `SLEEP_ABILITY_${enemy.Index}_${owner.Index}_${abil.Index}`)
	})
}
