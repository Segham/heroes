import { dotaunitorder_t, ExecuteOrder, Hero, Input, nevermore_shadowraze1, nevermore_shadowraze2, nevermore_shadowraze3, Unit, VKeys, VMouseKeys } from "wrapper/Imports"
import { EntityX, OrbwalkerMapX } from "X-Core/Imports"
import { HERO_DATA } from "../_Core_/data"
import { NevermoreCombo } from "../_Core_/Modules/Combo/Nevermore"
import { ComboCallback } from "../_Core_/Modules/Tick"
import { HeroService } from "../_Core_/Service/Hero"
import { SwitchPanelService } from "../_Core_/Service/SwicthPanel"
import { TargetCombo } from "../_Core_/Service/Target"
import { RazeDirectionState, ZXCMenu, ZXCPauseHeroState } from "./menu"
import { NevermoreAutoRaze } from "./Module/AutoRaze"
import { RazeDirection } from "./Module/RazeDirection"

export let ComboActived = false
let FastAblilitiesFilter: string[] = []

ZXCMenu.ComboKey.OnRelease(() => ComboActived = !ComboActived)

HeroService.RegisterHeroModule(ZXCMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onEnded,
	onKeyDown,
	onMouseDown,
})

ZXCMenu.BaseState.OnDeactivate(() => {
	EntityX.UnitsIsControllable.forEach(hero => {
		if (!HERO_DATA.ParticleManager.AllParticles.has(`ATTACK_TARGET_${hero.Index}`))
			return
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${hero.Index}`)
	})
})

function onTick(source: Unit) {
	if (!ZXCMenu.BaseState.value)
		return

	if (ZXCPauseHeroState.values.length < 5) {
		ZXCPauseHeroState.values = EntityX.EnemyHeroes.filter(x => x.IsValid && !x.IsIllusion).map(x => x.Name)
		ZXCPauseHeroState.Update()
	}

	const NameMenu = ZXCMenu.PFastSwitchAbilities
	FastAblilitiesFilter = NameMenu.values.filter(x => NameMenu.IsEnabled(x))

	ComboCallback(source, TargetCombo, ZXCMenu, ComboActived,
		() => NevermoreCombo(source, TargetCombo, ZXCMenu),
		() => NevermoreAutoRaze(source, TargetCombo, ZXCMenu),
	)
}

function onDraw(unit: Unit) {
	if (!ZXCMenu.BaseState.value || unit.IsIllusion)
		return

	SwitchPanelService.Draw(ZXCMenu, unit, ...FastAblilitiesFilter)

	if (TargetCombo === undefined
		|| !unit.IsAlive
		|| !unit.IsVisible
		|| !(TargetCombo instanceof Hero)
		|| !TargetCombo.IsAlive
		|| !TargetCombo.IsValid
		|| !TargetCombo.IsVisible
	) {
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${unit.Index}`)
		return
	}

	HERO_DATA.ParticleManager.DrawLineToTarget(`ATTACK_TARGET_${unit.Index}`, unit, TargetCombo,
		((ZXCMenu.StyleCombo.selected_id === 1 && !ComboActived) || (ZXCMenu.StyleCombo.selected_id === 0 && !ZXCMenu.ComboKey.is_pressed)
			? ZXCMenu.LineDeactive.selected_color
			: ZXCMenu.LineActive.selected_color
		),
	)
}

function onOrders(order: ExecuteOrder) {
	if (!ZXCMenu.BaseState.value)
		return true

	const owner = order.Issuers[0]
	const abil = order.Ability!

	if (RazeDirectionState.value && !Input.IsKeyDown(VKeys.CONTROL) && typeof abil !== "number" &&
		(abil instanceof nevermore_shadowraze1 || abil instanceof nevermore_shadowraze2 || abil instanceof nevermore_shadowraze3)) {
		return RazeDirection(abil, owner)
	}

	const Orbwalker = OrbwalkerMapX.get(owner)
	if (Orbwalker === undefined || !ZXCMenu.OrbwalkerSettings.State.value)
		return true

	Orbwalker.OnExecuteOrder(order, ZXCMenu.OrbwalkerSettings)

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& (ZXCMenu.StyleCombo.selected_id === 1 && ComboActived || (ZXCMenu.StyleCombo.selected_id === 0 && ZXCMenu.ComboKey.is_pressed)))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!ZXCMenu.BaseState.value)
		return true

	if (!Input.IsKeyDown(VKeys.CONTROL) && (ZXCMenu.ComboKey.assigned_key as VKeys) === key)
		return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!ZXCMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanelService.MouseLeftDown(ZXCMenu, ...FastAblilitiesFilter)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && (ZXCMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onEnded() {
	ZXCPauseHeroState.values = []
	ZXCPauseHeroState.Update()
	ComboActived = false
	FastAblilitiesFilter = []
}
