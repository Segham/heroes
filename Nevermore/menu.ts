import { Attributes, Menu } from "wrapper/Imports"
import { BaseHeroMenu } from "../menu"

export const ZXCMenu = BaseHeroMenu({
	NodeName: "Shadow Fiend",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	NpcName: "npc_dota_hero_nevermore",
	Ability: [
		"item_black_king_bar",
		"item_minotaur_horn",
		"item_blink",
		"item_sheepstick",
		"item_ethereal_blade",
		"item_abyssal_blade",
		"item_cyclone",
		"item_wind_waker",
		"item_spider_legs",
		"item_phase_boots",
		"nevermore_requiem",
		"item_shivas_guard",
		"item_refresher",
		"item_refresher_shard",
		"nevermore_shadowraze1",
		"nevermore_shadowraze2",
		"nevermore_shadowraze3",
	],
	DistBlink: { default: 150 },
})

export const ZXCPauseHeroState = ZXCMenu.SettingsTree
	.AddImageSelector("Auto pause (select hero)", [], new Map(), "Авто пауза на ультимейт")

export const RazeDirectionState = ZXCMenu.SettingsTree
	.AddToggle("Shadow Raze direction", true, "Use shadow raze in mouse direction")

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Auto pause (select hero)", "Авто пауза (выбирете героя)"],
	["Auto Pause on ulti", "Авто пауза на ультимейт"],
	["Shadow Raze direction", "Направленые Shadow Raze"],
	["Use shadow raze in mouse direction", "Использовать shadow raze на направление мыши"],
]))
