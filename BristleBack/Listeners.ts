import { dotaunitorder_t, ExecuteOrder, Input, Unit, VKeys, VMouseKeys } from "gitlab.com/FNT_Rework/wrapper/wrapper/Imports";
import { OrbwalkerMapX } from "gitlab.com/FNT_Rework/x-core/Imports";
import { HERO_DATA } from "../_Core_/data";
import { BaseCombo } from "../_Core_/Modules/Combo/Base";
import { ComboCallback } from "../_Core_/Modules/Tick";
import { HeroService } from "../_Core_/Service/Hero";
import { SwitchPanelService } from "../_Core_/Service/SwicthPanel";
import { TargetCombo } from "../_Core_/Service/Target";
import { BBMenu } from "./menu";

let ComboActived: boolean = false
let FastAblilitiesFilter: string[] = []

HeroService.RegisterHeroModule(BBMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onEnded,
	onKeyDown,
	onMouseDown,
})

BBMenu.ComboKey.OnRelease(() => ComboActived = !ComboActived)

function onTick(source: Unit) {
	if (!BBMenu.BaseState.value)
		return

	const NameMenu = BBMenu.PFastSwitchAbilities
	FastAblilitiesFilter = NameMenu.values.filter(x => NameMenu.IsEnabled(x))

	ComboCallback(source, TargetCombo, BBMenu, ComboActived,
		() => BaseCombo(source, TargetCombo, BBMenu),
	)
}

function onDraw(unit: Unit) {
	if (!BBMenu.BaseState.value || unit.IsIllusion)
		return

	SwitchPanelService.Draw(BBMenu, unit, ...FastAblilitiesFilter)

	if (TargetCombo === undefined
		|| !unit.IsAlive
		|| !TargetCombo.IsAlive
		|| !TargetCombo.IsValid
		|| !TargetCombo.IsVisible
	) {
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${unit.Index}`)
		return
	}

	HERO_DATA.ParticleManager.DrawLineToTarget(`ATTACK_TARGET_${unit.Index}`, unit, TargetCombo,
		((BBMenu.StyleCombo.selected_id === 1 && !ComboActived) || (BBMenu.StyleCombo.selected_id === 0 && !BBMenu.ComboKey.is_pressed)
			? BBMenu.LineDeactive.selected_color
			: BBMenu.LineActive.selected_color
		),
	)
}

function onOrders(order: ExecuteOrder) {
	if (!BBMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	const Orbwalker = OrbwalkerMapX.get(order.Issuers[0])
	if (Orbwalker === undefined)
		return true

	Orbwalker.OnExecuteOrder(order, BBMenu.OrbwalkerSettings)

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& (BBMenu.StyleCombo.selected_id === 1 && ComboActived || (BBMenu.StyleCombo.selected_id === 0 && BBMenu.ComboKey.is_pressed)))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!BBMenu.BaseState.value)
		return true

	if (!Input.IsKeyDown(VKeys.CONTROL) && (BBMenu.ComboKey.assigned_key as VKeys) === key)
		return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!BBMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanelService.MouseLeftDown(BBMenu, ...FastAblilitiesFilter)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && (BBMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onEnded() {
	ComboActived = false
	FastAblilitiesFilter = []
}
