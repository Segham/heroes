import { BaseHeroMenu } from "../menu";

export const BBMenu = BaseHeroMenu({
	NodeName: "Bristleback",
	NpcName: "npc_dota_hero_bristleback",
	Ability: [
		"item_blink",
		"item_black_king_bar",
		"item_minotaur_horn",
		"item_sheepstick",
		"item_abyssal_blade",
		"item_orchid",
		"item_bloodthorn",
		"item_diffusal_blade",
		"item_nullifier",
		"item_medallion_of_courage",
		"item_solar_crest",
		"bristleback_viscous_nasal_goo",
		"bristleback_quill_spray",
		"item_shivas_guard",
		"item_mask_of_madness",
	],
})
