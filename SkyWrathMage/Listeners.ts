import { dotaunitorder_t, ExecuteOrder, Input, Unit, VKeys, VMouseKeys } from "wrapper/Imports";
import { OrbwalkerMapX } from "X-Core/Imports";
import { HERO_DATA } from "../_Core_/data";
import { SkyWrathCombo } from "../_Core_/Modules/Combo/SkyWrath";
import { ComboCallback, RunTimeCallback } from "../_Core_/Modules/Tick";
import { HeroService } from "../_Core_/Service/Hero";
import { SwitchPanelService } from "../_Core_/Service/SwicthPanel";
import { TargetCombo } from "../_Core_/Service/Target";
import { HarassKey, HarassOrbwalker, SkyMenu } from "./menu";
import { SkyWrathHarassMode } from "./Module/Harass";

let ComboActived: boolean = false
let FastAblilitiesFilter: string[] = []

SkyMenu.ComboKey.OnRelease(() => ComboActived = !ComboActived)

HeroService.RegisterHeroModule(SkyMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onEnded,
	onKeyDown,
	onMouseDown,
})

function onTick(source: Unit) {
	if (!SkyMenu.BaseState.value)
		return

	const NameMenu = SkyMenu.PFastSwitchAbilities
	FastAblilitiesFilter = NameMenu.values.filter(x => NameMenu.IsEnabled(x))

	ComboCallback(source, TargetCombo, SkyMenu, ComboActived,
		() => SkyWrathCombo(source, TargetCombo, SkyMenu),
	)

	RunTimeCallback(source, TargetCombo, SkyMenu, HarassKey.is_pressed, HarassOrbwalker.value,
		() => SkyWrathHarassMode(source, TargetCombo, SkyMenu),
	)
}

function onDraw(unit: Unit) {
	if (!SkyMenu.BaseState.value || unit.IsIllusion)
		return

	SwitchPanelService.Draw(SkyMenu, unit, ...FastAblilitiesFilter)

	if (TargetCombo === undefined
		|| !unit.IsAlive
		|| !TargetCombo.IsAlive
		|| !TargetCombo.IsValid
		|| !TargetCombo.IsVisible
	) {
		HERO_DATA.ParticleManager.DestroyByKey(`ATTACK_TARGET_${unit.Index}`)
		return
	}

	HERO_DATA.ParticleManager.DrawLineToTarget(`ATTACK_TARGET_${unit.Index}`, unit, TargetCombo,
		((SkyMenu.StyleCombo.selected_id === 1 && !ComboActived) || (SkyMenu.StyleCombo.selected_id === 0 && !SkyMenu.ComboKey.is_pressed)
			? SkyMenu.LineDeactive.selected_color
			: SkyMenu.LineActive.selected_color
		),
	)
}

function onOrders(order: ExecuteOrder) {
	if (!SkyMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	const Orbwalker = OrbwalkerMapX.get(order.Issuers[0])
	if (Orbwalker === undefined)
		return true

	Orbwalker.OnExecuteOrder(order, SkyMenu.OrbwalkerSettings)

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& HarassKey.is_pressed
		&& HarassOrbwalker.value
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	if (TargetCombo !== undefined
		&& TargetCombo.IsAlive
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		&& (SkyMenu.StyleCombo.selected_id === 1 && ComboActived || (SkyMenu.StyleCombo.selected_id === 0 && SkyMenu.ComboKey.is_pressed)))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!SkyMenu.BaseState.value)
		return true

	if (!Input.IsKeyDown(VKeys.CONTROL) && ((SkyMenu.ComboKey.assigned_key as VKeys) === key || (HarassKey.assigned_key as VKeys) === key))
		return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!SkyMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanelService.MouseLeftDown(SkyMenu, ...FastAblilitiesFilter)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && ((SkyMenu.ComboKey.assigned_key as VMouseKeys) === key || (HarassKey.assigned_key as VMouseKeys) === key))
		return false

	return true
}

function onEnded() {
	ComboActived = false
	FastAblilitiesFilter = []
}
