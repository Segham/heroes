import { Attributes, Menu } from "wrapper/Imports";
import { BaseHeroMenu } from "../menu";

export const SkyMenu = BaseHeroMenu({
	NodeName: "Skywrath Mage",
	NpcName: "npc_dota_hero_skywrath_mage",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Ability: [
		"item_blink",
		"item_black_king_bar",
		"item_minotaur_horn",
		"item_sheepstick",
		"item_rod_of_atos",
		"item_veil_of_discord",
		"item_ethereal_blade",
		"item_dagon_5",
		"skywrath_mage_ancient_seal",
		"skywrath_mage_concussive_shot",
		"skywrath_mage_mystic_flare",
		"skywrath_mage_arcane_bolt",
		"item_orchid",
		"item_bloodthorn",
		"item_shivas_guard",
	],
	ExtendLinkenBreak: [
		"skywrath_mage_ancient_seal",
		"skywrath_mage_arcane_bolt",
	],
	DistBlink: {default: 450},
})

const HarassTree = SkyMenu.BaseTree.AddNode("Harass")
export const HarassOrbwalker = HarassTree.AddToggle("State orbwalker", false, "State orbwalker for harass")
export const HarassKey = HarassTree.AddKeybind("Key", "D", "While using Spam, your hero will constantly use Arcane Bolt & Orbwalker")

export const DoubleUlti = SkyMenu.SettingsTree.AddToggle("Double ult", true,
	"If you possess an aghanim (buff/item)\nwhen you ultimate both of the abilities will be casted at the enemy hero",
)

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Harass", "Спам"],
	["State orbwalker", "Состояние orbwalker'a"],
	["State orbwalker for harass", "Состояние orbwalker'a для спама"],
	["Double ult", "Двойной Ультимейт"],
	[
		"If you possess an aghanim (buff/item)\nwhen you ultimate both of the abilities will be casted at the enemy hero",
		"Если у вас есть аганим (баф/предмет)\nто будет использоваться двойная ультимативная способность на противника",
	],
	[
		"While using Spam, your hero will constantly use Arcane Bolt & Orbwalker",
		"При использовании Спама ваш герой будет постоянно использовать Arcane Bolt и Orbwalker",
	],
]))
