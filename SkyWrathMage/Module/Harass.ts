import { skywrath_mage_arcane_bolt, Unit } from "wrapper/Imports";
import { MenuCombo } from "../../menu";
import { HERO_DATA } from "../../_Core_/data";
import { HERO_UTILITY } from "../../_Core_/Service/Utills";

export const SkyWrathHarassMode = (owner: Unit, enemy: Nullable<Unit>, menu: MenuCombo) => {

	if (enemy === undefined
		|| owner.IsInAbilityPhase
		|| enemy.HasBuffByName(HERO_DATA.WinterCurse))
		return false

	return HERO_UTILITY.GetAbility(owner, menu).filter(x => x !== undefined && x.CanBeCasted()).some(abil => {
		if (!(abil instanceof skywrath_mage_arcane_bolt))
			return false

		const sleepAnyAbility = `SLEEP_ABILITY_${enemy.Index}_${owner.Index}_${abil.Index}`

		if (HERO_DATA.Sleeper.Sleeping(sleepAnyAbility))
			return false

		abil.UseAbility(enemy)
		HERO_DATA.Sleeper.Sleep(abil.GetCastDelay(enemy.Position, true) + 50, sleepAnyAbility)
		return true
	})
}
